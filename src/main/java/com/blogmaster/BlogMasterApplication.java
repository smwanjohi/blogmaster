package com.blogmaster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@ImportResource("classpath:Beans.xml")

public class BlogMasterApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogMasterApplication.class, args);
	}
}
