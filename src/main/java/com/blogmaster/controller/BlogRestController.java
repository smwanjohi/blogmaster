package com.blogmaster.controller;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.blogmaster.model.Blog;
import com.blogmaster.security.auth.jwt.JwtTokenUtil;
import com.blogmaster.security.auth.jwt.JwtUser;
import com.blogmaster.service.BlogService;

;

@RestController
public class BlogRestController {
private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private BlogService blogService;
	
	@Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;
	
	
	
	@RequestMapping(value="/blogs",
					method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	
	public ResponseEntity<Collection<Blog>> getBlogs (){
		
		Collection<Blog> blogs= blogService.findAll();
		
		logger.info("Returning all blog post to user");
		return new ResponseEntity<Collection<Blog>>(blogs,HttpStatus.OK);
		
	}
	
	
	@RequestMapping(value="/blogs/{id}",
			method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Blog> getBlog (@PathVariable("id") Long id){
		Blog blog = blogService.findOne(id);
		if(blog == null){
			logger.info("Post no found");
			return new ResponseEntity<Blog>(HttpStatus.NOT_FOUND);
			
		}
		logger.info("Requested blog post found");
		return new ResponseEntity<Blog>(blog,HttpStatus.OK);
	}
	
	@RequestMapping(value="/blogs", 
			method=RequestMethod.POST, 
					consumes=MediaType.APPLICATION_JSON_VALUE,
			produces=MediaType.APPLICATION_JSON_VALUE 
			)
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Blog> createBlog (@RequestBody Blog blog,HttpServletRequest request){
		String token = request.getHeader(tokenHeader);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);
        blog.setUserId(user.getId());
		Blog savedBlog = blogService.create(blog);
		logger.info("Admin successfully created new post");
		return new ResponseEntity<Blog>(savedBlog, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/blogs/{id}", 
			method=RequestMethod.PUT, 
					consumes=MediaType.APPLICATION_JSON_VALUE,
			produces=MediaType.APPLICATION_JSON_VALUE 
			)
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Blog> updateBlog (@RequestBody Blog blog, HttpServletRequest request){
		String token = request.getHeader(tokenHeader);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);
        blog.setUserId(user.getId());
		Blog updatedBlog = blogService.update(blog);
		if(updatedBlog== null){
			return new ResponseEntity<Blog>(HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		logger.info("Admin successfully updated post");
		return new ResponseEntity<Blog>(updatedBlog, HttpStatus.CREATED);
		
	}
	
	@RequestMapping(value="/blogs/{id}", 
			method=RequestMethod.DELETE
			)
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Blog> deleteBlog (
			@PathVariable("id") Long id){
		blogService.delete(id);
		logger.info("Admin successfully deleted post");
		return new ResponseEntity<Blog>(HttpStatus.NO_CONTENT);
		
	}
	
	

}
