package com.blogmaster.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blogmaster.model.Blog;

@Repository
public interface BlogRepository extends JpaRepository<Blog, Long> {

}
