package com.blogmaster.security.auth.jwt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;


import com.blogmaster.model.Role;
import com.blogmaster.model.User;


public final class JwtUserFactory {

    private JwtUserFactory() {
    }

    public static JwtUser create(User user) {
    	Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
		for(Role role : user.getRoles()){
			grantedAuthorities.add(new SimpleGrantedAuthority(role.getCode()));
			
			
		}
        return new JwtUser(
                user.getId(),
                user.getUsername(),
                user.getName(),
                user.getEmail(),
                user.getPassword(),
                grantedAuthorities,
                user.getEnabled()
               
        );
    }

    
}
