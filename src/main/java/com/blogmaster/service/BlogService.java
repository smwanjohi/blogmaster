package com.blogmaster.service;

import java.util.Collection;

import com.blogmaster.model.Blog;

public interface BlogService {
	
	Collection <Blog> findAll();
	
	Blog findOne(Long id);
	
	Blog create (Blog blog);
	
	Blog update (Blog blog);
	
	void delete (Long id);
	

}
