package com.blogmaster.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import com.blogmaster.model.Blog;
import com.blogmaster.repository.BlogRepository;



@Service
public class BlogServiceBean implements BlogService {
	
	@Autowired
	private BlogRepository blogRepo;

	@Override
	
	public Collection<Blog> findAll() {
		Collection<Blog> blogs=blogRepo.findAll();
		return blogs;
	}

	@Override
	public Blog findOne(Long id) {
		Blog blog=blogRepo.findOne(id);
		
		return blog;
	}

	@Override
	public Blog create(Blog blog) {
		if(blog.getId() != null){
			//id already exists
			return null;
		}
		Blog savedBlog = blogRepo.save(blog);
		return savedBlog;
		
	}

	@Override
	public Blog update(Blog blog) {
		Blog blogPersisted = findOne(blog.getId());
		if(blogPersisted == null){
			//not found
			return null;
		}
		Blog updatedBlog = blogRepo.save(blog);
		return updatedBlog;
	}

	@Override
	public void delete(Long id) {
		blogRepo.delete(id);

	}

}
