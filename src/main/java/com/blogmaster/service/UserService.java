package com.blogmaster.service;

import com.blogmaster.model.User;

public interface UserService {
	User findByUsername(String username);

}
