package com.blogmaster.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blogmaster.model.User;
import com.blogmaster.repository.UserRepository;

@Service
public class UserServiceBean implements UserService {

	@Autowired
	private UserRepository userRepo;
	
	@Override
	public User findByUsername(String username) {
	User user =	userRepo.findByUsername(username);
		return user;
	}

}
