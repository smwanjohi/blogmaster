--
-- Dumping data for table `User`
-- password for both profiles is pass123





INSERT INTO Role (id, code, label) VALUES
(1, 'ROLE_ADMIN', 'administrator'),
(2, 'ROLE_USER', 'user');

INSERT INTO User ( username, email, password, name, enabled) VALUES
('admin', 'smathenge10@gmail.com', '$2a$06$UY4EjNpS65cY059gE84TAODIBH.VrsLcbVzDTWmorTLutS/dhQp7W', 'simon mathenge', 1),
( 'user', 'simon.mathenge@bluesky.co.ke', '$2a$06$UY4EjNpS65cY059gE84TAODIBH.VrsLcbVzDTWmorTLutS/dhQp7W', 'simon wanjohi', 1);

INSERT INTO Blog ( title, body, userId) VALUES
( 'Using Apache camel', 'Apache gives you out of the box features for your application. Enjoy the tutorial.', (SELECT id FROM User where username= 'admin')),
( 'Spring boot', 'Spring boot is a popular java framework that is used to implement secure RESTFUL web service', (SELECT id FROM User where username= 'admin')),
( 'blogmaster', 'Blogmaster is a web application that implements RESTful web services secured by a web token', (SELECT id FROM User where username= 'admin'));


INSERT INTO AccountRole (userId, roleId) VALUES
(1,1),(2,2);
--INSERT INTO AccountRole (userId, roleId) VALUES
--SELECT a.id, r.id FROM User a, Role r WHERE a.username = 'user' and r.id = 2;
--INSERT INTO AccountRole (userId, roleId) VALUES
--SELECT a.id, r.id FROM User a, Role r WHERE a.username = 'admin' and r.id = 1;


