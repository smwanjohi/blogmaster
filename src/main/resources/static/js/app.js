(function(){
	"use strict";
	angular.module("blogApp",["ui.router"])
	.config(function($stateProvider, $urlRouterProvider,$httpProvider, $provide){

        
        $stateProvider.state("auth",{
        					url:"/auth",
        					templateUrl: 'views/authView.html',
        					controller:"authController as auth"})
        				.state("users",{
        					url:"/users",
        					templateUrl: 'views/userView.html',
        					controller:"userController as user"});
        
        $urlRouterProvider.otherwise(function ($injector, $location) {
            var $state = $injector.get("$state");
            $state.go("auth");
        });
		
	}).run(function($rootScope, $state){
		$rootScope.$on('$stateChangeStart', function (event, toState) {

            // Grab the user from local storage and parse it to an object
            var user = JSON.parse(localStorage.getItem('user'));
            var token= localStorage.getItem('satellizer_token');
            if(token){
            	$rootScope.token=token;
            }
            // If there is any user data in local storage then the user is quite
            // likely authenticated. If their token is expired, or if they are
            // otherwise not actually authenticated, they will be redirected to
            // the auth state because of the rejected request anyway
            if (user) {
                // The user's authenticated state gets flipped to
                // true so we can now show parts of the UI that rely
                // on the user being logged in
                $rootScope.authenticated = true;
                // Putting the user's data on $rootScope allows
                // us to access it anywhere across the app. Here
                // we are grabbing what is in local storage
                $rootScope.currentUser = user;
                // If the user is logged in and we hit the auth route we don't need
                // to stay there and can send the user to the main state
                

                if (toState.name === "auth") {

                    // Preventing the default behavior allows us to use $state.go
                    // to change states
                    event.preventDefault();
                    // go to the "main" state which in our case is users
                    $state.go('users');
                }


            }
        });
		
	});
})();