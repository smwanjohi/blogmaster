(function(){
	"use strict";
	angular.module("blogApp")
	.controller("authController",authController);
	function authController($http,$rootScope,$state){
		var vm = this;
		
		vm.login=function(){
			var credentials= {
					username:vm.username,
					password:vm.password
			};
			$http.post('/auth',credentials).success(function(data){
				console.log(data);
				$rootScope.token=data.token;
				localStorage.setItem('satellizer_token', data.token);
				$http.get('/user',{headers:{'Authorization':''+data.token}}).success(function(data){
					
					$rootScope.currentUser=data;
					$rootScope.currentUser.role=data.authorities[0].authority;
					$rootScope.currentUser.authorities='';
					console.log($rootScope.currentUser);
					var user = JSON.stringify($rootScope.currentUser);
					localStorage.setItem('user',user);
					$state.go('users');
				});
				
			});
			
//			$auth.login(credentials).then(function() {
//
//				// Return an $http request for the now authenticated
//				// user so that we can flatten the promise chain
//				return $http.get('/user');
//
//			// Handle errors
//			}, function(error) {
//				vm.loginError = true;
//				vm.loginErrorText = error.data.error;
//
//			// Because we returned the $http.get request in the $auth.login
//			// promise, we can chain the next promise to the end here
//			}).then(function(response) {
//				console.log(response);
//				// Stringify the returned data to prepare it
//				// to go into local storage
//				var user = JSON.stringify(response.data.user);
//
//				// Set the stringified user data into local storage
//				localStorage.setItem('user', user);
//
//				// The user's authenticated state gets flipped to
//				// true so we can now show parts of the UI that rely
//				// on the user being logged in
//				$rootScope.authenticated = true;
//
//				// Putting the user's data on $rootScope allows
//				// us to access it anywhere across the app
//				$rootScope.currentUser = response.data.user;
//
//				// Everything worked out so we can now redirect to
//				// the users state to view the data
//				$state.go('users');
//			});
		}
		
	};
})();