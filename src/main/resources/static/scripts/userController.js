(function(){
	"use strict";
	angular.module("blogApp")
	.controller("userController",userController);
	function userController($http,$rootScope,$state){
		var vm =this;
		
		
		//this request fetches all the blogs
		$http.get('/blogs',{
			headers:{'Authorization':''+$rootScope.token}
		}).success(function(data){
			vm.blogs=data;
			console.log(vm.blogs);
			
		});
		
		
		//function to expand a blog post
		vm.read=function(blog){
			vm.view=true;
			vm.createblog=false;
			vm.edit=false;
			vm.blog=blog;
		};
		
		
		//function to edit a blog post
		vm.deleteBlog=function(blog){
			var index=vm.blogs.indexOf(blog);
			if(confirm("Delete this post?")){
				$http.delete('/blogs/'+blog.id,{
					headers:{'Authorization':''+$rootScope.token}
				}).success(function(){
					vm.blogs.splice(index,1);
					alert('Blog deleted!');
					
				});
			}
			
		}
		
		//function to edit a post
		vm.editPost=function(){
			var index=vm.blogs.indexOf(vm.editblog);
			$http.put('/blogs/'+vm.editblog.id,vm.editblog,{
				headers:{'Authorization':''+$rootScope.token}
			}).success(function(){
				alert('Blog edited!');
				vm.blogs[index]=vm.editblog;
				vm.edit=false;
			});
		}
		//function to create a new post
		vm.create=function(){
			var blog={title:vm.title,
						body:vm.body,
						userId:$rootScope.currentUser.id
						};
			$http.post('/blogs',blog,{
				headers:{'Authorization':''+$rootScope.token}
			}).success(function(data){
				alert('Blog entry added!');
				vm.blogs.push(data);
				vm.createblog=false;
				
			});
			}
		
		//function to log a user out
		vm.logout=function(){
			// Remove the authenticated user from local storage
			localStorage.removeItem('user');
			localStorage.removeItem('satellizer_token');
			// Flip authenticated to false so that we no longer
			// show UI elements dependant on the user being logged in
			$rootScope.authenticated = false;
			
			// Remove the current user info from rootscope
			$rootScope.currentUser = null;
			$state.go("auth");
		}
		
		
		
		
	};
})();