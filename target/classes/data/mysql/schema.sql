
DROP TABLE IF EXISTS AccountRole;
DROP TABLE IF EXISTS Blog;
DROP TABLE IF EXISTS Role;
DROP TABLE IF EXISTS User;

CREATE TABLE IF NOT EXISTS Role (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `label` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS User (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(60) NOT NULL,
  `name` varchar(40) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS AccountRole (
  `userId` bigint(11) NOT NULL,
  `roleId` bigint(11) NOT NULL,
  PRIMARY KEY (userId,roleId),
  CONSTRAINT `AccountRole_ibfk_1` FOREIGN KEY (`userId`) REFERENCES User (`id`) ON DELETE CASCADE,
  CONSTRAINT `AccountRole_ibfk_2` FOREIGN KEY (`roleId`) REFERENCES Role (`id`) ON DELETE CASCADE
);



CREATE TABLE IF NOT EXISTS Blog (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `body` varchar(100) NOT NULL,
  `userId` bigint(11) NOT NULL,
  PRIMARY KEY (`id`,userId),
  
  CONSTRAINT `Blog_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `User` (`id`) ON DELETE CASCADE
);

--
-- Constraints for table `AccountRole`
--
--ALTER TABLE AccountRole
 -- ADD CONSTRAINT `AccountRole_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `User` (`id`) ON DELETE CASCADE,
-- ADD CONSTRAINT `AccountRole_ibfk_2` FOREIGN KEY (`roleId`) REFERENCES `Role` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `Blog`
--
--ALTER TABLE Blog
-- ADD CONSTRAINT `Blog_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `User` (`id`) ON DELETE CASCADE;